/*
Simple Task Manager w/o libraries
Version: 1.0.1
Created by Witty
*/
document.addEventListener('DOMContentLoaded', (e) => {

    // Check DB
    window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
    // Open DB
    let request = indexedDB.open('TaskManager', 1);
    // onupgradeneeded (event) - Running when new DB creating or current DB updating.
    request.onupgradeneeded = (event) => {
            let DB = event.target.result;
            // Check current Obj in DB
            if (!DB.objectStoreNames.contains('Tasks')) {
                // Creating Store Object "Tasks" and assignment value "id" as primary key.
                // autoIncrement - keys generator lets create unique value for each Obj
                // options - Obj with Parameters wherein you can indicate base key for manage DB properties. 
                var ObjectStore = DB.createObjectStore('Tasks', { keyPath: 'id', autoIncrement: true });

                ObjectStore.createIndex('IndexByTask', 'task', { unique: true });
                ObjectStore.createIndex('IndexByEmail', 'email', { unique: false });
            }
        }
        // callback for handle success / errors
    request.onsuccess = (event) => {
        console.log('Success! DB has been opened!');
        // Get DB
        db = event.target.result;
        showTasks();
    }
    request.onerror = (event) => {
        console.log('Error! There is problem with opening your DB');
    }


});
// Add tasks function
function addTask(e) {
    let task = $('#task').val();
    let person = $('#person').val();
    let email = $('#email').val();
    let date = $('#date').val();
    // New transaction for ref to ObjStore
    let transaction = db.transaction(['Tasks'], 'readwrite'); //readonly
    // Ref to the table
    let store = transaction.objectStore('Tasks');
    // Create task
    let Task = {
        task: task,
        person: person,
        email: email,
        date: date
    };
    // Ref to add
    let req = store.add(Task);
    req.onsuccess = (event) => {
        alert('New task was added');
        window.location.replace('index.html');
    };
    req.onerror = (event) => {
        alert('There is a problem with adding a new task');
        return false;
    };

}
// Show tasks function
function showTasks(event) {
    // Creating transaction
    let transaction = db.transaction(['Tasks'], 'readonly');
    let store = transaction.objectStore('Tasks');
    // Ref to existing Obj in DB
    let index = store.index('IndexByTask');
    let output = '';
    // Creating a special label for defining elements within the database at a given index
    // Cursor is choosing each Obj in ObjStore or indexing in sequence, let you do something with choosen data.
    index.openCursor().onsuccess = (event) => {
        let cursor = event.target.result;
        if (cursor) {
            output += `<tr class='task_${cursor.value.id}'>`;
            output += `<td><span>${cursor.value.id}</span></td>`;
            output += `<td><span>${cursor.value.task}</span></td>`;
            output += `<td><span class='cursor task' contenteditable='true' data-field='person' data-id='${cursor.value.id}'>${cursor.value.person}</span></td>`;
            output += `<td><span class='cursor task' contenteditable='true' data-field='email' data-id='${cursor.value.id}'>${cursor.value.email}</span></td>`;
            output += `<td><span class='cursor task' contenteditable='true' data-field='date' data-id='${cursor.value.id}'>${cursor.value.date}</span></td>`;
            output += `<td><a onclick="deleteTask(${cursor.value.id})" class="btn btn-danger" href=''>Delete</a></td>`;
            output += '</tr>';
            // continue before elements end
            cursor.continue();
        }
        $('#tasks').html(output);
    }
}

// Delete all users
function deleteTasks() {
    indexedDB.deleteDatabase('TaskManager');
    window.location.replace('./index.html');
}
// Delete user
function deleteTask(id) {
    let transaction = db.transaction(['Tasks'], 'readwrite');
    let store = transaction.objectStore('Tasks');
    let req = store.delete(id);
    req.onsuccess = (event) => {
        alert(`Task #${id} was deleted`);
        // Delete element from DOM
        $(`.task_${id}`).remove();
        return false;
    };
    req.onerror = (event) => {
        alert('There is a problem with deleting this task');
        return false;
    };
}
// Updating user information
$('#tasks').on('blur', ".task", function() {
    // Calculating updated text
    let newText = $(this).html();
    // Finding fields
    let Field = $(this).data('field');
    let TaskID = $(this).data('id');
    let transaction = db.transaction(['Tasks'], 'readwrite');
    let store = transaction.objectStore('Tasks');
    let req = store.get(TaskID);
    req.onsuccess = (e) => {
        let data = req.result;
        if (Field === 'person') {
            data.person = newText;
        } else if (Field === 'email') {
            data.email = newText
        } else if (Field === 'date') {
            data.date = newText
        }
        console.log(data);
        let reqUpdate = store.put(data);
        reqUpdate.onsuccess = () => {
            console.log('Task field updated!');
        }
        reqUpdate.onerror = () => {
            console.log('Task field not updated!');
        }
    }
    req.onerror = (e) => {
        conconsole.log('No nection to DB');
    }

});